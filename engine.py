#standard stuff
import pygame
import time

pygame.init()

display = pygame.display.set_mode((800,600))
clock = pygame.time.Clock()

timerd = -1
b = 0
i = 0
noright = False
noleft = False

#colors
black = (0,0,0)
white = (255,255,255)
red = (255,0,0)
blue = (0,255,0)
green = (0,0,255)
yellow = (255,0,255)
orange = (255,128,0)
teal = (0,255,255)
purple = (255,255,0)

#static init code
#todo: make sprites
player = pygame.image.load('player.png')
scp_173 = pygame.image.load('173.png')
scp_106 = pygame.image.load('106.png')
scp_049 = pygame.image.load('049.png')
background = pygame.image.load('background.png')

#scp vars
#173
scp_173_view = 0
scp_173_x = 600
scp_173_y = 20
scp_173_xch = 0
scp_173_ych = 0
#106
scp_106_view = 0
scp_106_x = 400
scp_106_y = 20
scp_106_xch = 0
scp_106_ych = 0
scp_106_xov = False
scp_106_yov = False
#049
scp_049_view = 0
scp_049_x = 800
scp_049_y = 20
scp_049_xch = 0
scp_049_ych = 0
scp_049_xov = False
scp_049_yov = False

#player vars
player_view = 0
player_x = 350
player_y = 20
player_xa = 350
player_ya = 20
player_xch = 0
player_ych = 0
player_clr = 0
player_health = 5
player_invin_timer = 0
lx = 0
ly = 0

#background vars
background_x = 0
background_y = 0

done = False

#functions
def move(object,x,y):
	display.blit(object, (x,y))

#make a door function
def mkd(x,y,clear=0,heavy=False):
	global i
	name = "door"+str(i)
	if heavy:
		exec(name+" = pygame.image.load('hdoor.png')", globals())
	else:
		exec(name+" = pygame.image.load('door.png')", globals())
	xn = name+"_x = "+str(x)
	exec(xn, globals())
	yn = name+"_y ="+str(y)
	exec(yn, globals())
	ychn = name+"_ych = 0"
	exec(ychn, globals())
	clear = name+"_clr = "+str(clear)
	exec(clear, globals())
	exec("timerd_"+name+" = -1", globals())
	exec(name+"_button = 0", globals())
	i += 1

def doorupdate():
	global timerd
	global timerd2
	global player_xch
	global player_ych
	global player_x
	global player_y
	global player_clr
	global i
	global b
	global noleft
	global noright
	global done
	global scp_049_xov
	b = 0
	while b < i:
		move(globals()["door"+str(b)],globals()["door"+str(b)+"_x"],globals()["door"+str(b)+"_y"])
		door = "door"+str(b)
		globals()[door+"_x"] += player_xch
		globals()[door+"_y"] += player_ych
		move(globals()[door],globals()[door+"_x"],globals()[door+"_y"])
		if player_x > globals()[door+"_x"]-7 and player_x < globals()[door+"_x"] and player_y == globals()[door+"_y"] or globals()[door+"_button"] == True:
			if player_clr < globals()[door+"_clr"] and globals()[door+"_button"] == False:
				player_xch = 0
				noright = True
			else:
				if globals()["timerd_"+door] == -1:
					globals()["timerd_"+door] = 60
					globals()[door+"_y"] -= 70
					globals()[door+"_button"] = False
		if player_x < globals()[door+"_x"]-7:
			noright = False
		if player_x < globals()[door+"_x"]+7 and player_x > globals()[door+"_x"] and player_y == globals()[door+"_y"]:
			if player_clr < globals()[door+"_clr"]:
				player_xch = 0
				noleft = True
				done = True
			else:
				if globals()["timerd_"+door] == -1:
					globals()["timerd_"+door] = 60
					globals()[door+"_y"] -= 70
		if player_x > globals()[door+"_x"]+15 and done == False:
			noleft = False

		if timerd == 0:
			player_xch = 0
		if globals()["timerd_"+door] > -1:
			globals()["timerd_"+door] -= 1
		if globals()["timerd_"+door] == 0:
			globals()[door+"_y"] += 70
		if scp_049_x < globals()[door+"_x"]+10 and scp_049_x > globals()[door+"_x"] and scp_049_y == globals()[door+"_y"]:
			scp_049_xov = True
		if scp_049_x > globals()[door+"_x"]-10 and scp_049_x < globals()[door+"_x"] and scp_049_y == globals()[door+"_y"]:
			scp_049_xov = True
		b += 1

#create doors
mkd(202,20,3,False) #0
mkd(77,20,0,False) #1
mkd(-31,20,6,False) #2

#called when up arrow is pressed
def interact():
	global player_x
	global player_y
	global player_xch
	global player_ych
	global lx
	global ly
	if player_x < 370+lx and player_x > 330+lx and player_y == 20+ly:
		if globals()["door0_button"]:
			globals()["door0_button"] = False
		else:
			globals()["door0_button"] = True

#draw static objects onscreen (ie. decoration, unlinked sprites, etc)
#runs every frame
def drawstatic():
	#format
	#[object] = pygame.image.load("[sprite]")
	#move([object],[x],[y])
	button1 = pygame.image.load("button.png")
	move(button1,370+lx,40+ly)

#crash handler
crashed = False
wait = 0

while not crashed:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			crashed = True
			print("crash")
		#controls
		if event.type == pygame.KEYDOWN:
			#debug option to create a door
			if event.key == pygame.K_BACKQUOTE:
				mkd(player_x+20,player_y,1)
			elif event.key == pygame.K_UP:
				interact()
			elif event.key == pygame.K_RIGHT:
				if noright == False:
					player_xch = -5
				if player_view == 1:
					player = pygame.transform.flip(player,True,False)
				player_view = 0
			elif event.key == pygame.K_LEFT:
				if noleft == False:
					player_xch = 5
				if player_view == 0:
					player = pygame.transform.flip(player,True,False)
				player_view = 1
		if event.type == pygame.KEYUP:
			if event.key == pygame.K_RIGHT or event.key == pygame.K_LEFT:
				player_xch = 0
	#updating code
	display.fill(black)
	scp_173_xch = 0
	#door movement timer its important
	if timerd > -1:
		timerd = timerd-1
	#invincibilty timer
	if player_invin_timer > 0:
		player_invin_timer -= 1
	#scp 173 movement timer
	if wait > 0:
		wait = wait-1
	#scp 173 movement code
	if abs(scp_173_x) > abs(player_x)-200 and abs(scp_173_x) < abs(player_x) and wait == 0 and player_view == 0:
		if scp_173_view == 1:
			scp_173 = pygame.transform.flip(scp_173,True,False)
			scp_173_view = 0
		scp_173_x = player_x
		#play neck snap sound here
		time.sleep(1)
		player_health -= 5
	elif abs(scp_173_x) > abs(player_x) and abs(scp_173_x) < abs(player_x)+200 and wait == 0 and player_view == 1:
		if scp_173_view == 0:
			scp_173 = pygame.transform.flip(scp_173,True,False)
			scp_173_view = 1
		scp_173_x = player_x
		#play neck snap sound here
		time.sleep(1)
		player_health -= 5
	elif scp_173_x < player_x and wait == 0 and player_view == 0:
		if scp_173_view == 1:
			scp_173 = pygame.transform.flip(scp_173,True,False)
			scp_173_view = 0
		scp_173_xch = 200
	elif scp_173_x > player_x and wait == 0 and player_view == 1:
		if scp_173_view == 0:
			scp_173 = pygame.transform.flip(scp_173,True,False)
			scp_173_view = 1
		scp_173_xch = -200
	#049 movement code
	if scp_049_x < player_x-1:
		scp_049_xch = 2
		if scp_049_view == 1:
			scp_049 = pygame.transform.flip(scp_049,True,False)
			scp_049_view = 0
	elif scp_049_x > player_x+1:
		scp_049_xch = -2
		if scp_049_view == 0:
			scp_049 = pygame.transform.flip(scp_049,True,False)
			scp_049_view = 1
	elif player_invin_timer == 0:
		player_health -= 1
		player_invin_timer = 180
	#106 movement code
	if scp_106_x < player_x:
		scp_106_xch = 1
		if scp_106_view == 1:
			scp_106 = pygame.transform.flip(scp_106,True,False)
			scp_106_view = 0
	elif scp_106_x > player_x:
		scp_106_xch = -1
		if scp_106_view == 0:
			scp_106 = pygame.transform.flip(scp_106,True,False)
			scp_106_view = 1
	elif player_invin_timer == 0:
		player_health -= 1
		player_invin_timer = 180
	scp_173_x += scp_173_xch+player_xch
	scp_173_y += scp_173_ych+player_ych
	background_x += player_xch
	background_y += player_ych
	#draw background
	move(background,background_x,background_y)
	#draw static objects defined in drawstatic()
	drawstatic()
	if player_health < 1:
		print("You died.")
		quit()
	#draw player
	if player_invin_timer % 2 == 0:
		move(player,player_x,player_y)
	#draw 173
	move(scp_173,scp_173_x,scp_173_y)
	#draw doors
	doorupdate()
	if not scp_049_xov:
		scp_049_x += scp_049_xch
	if not scp_049_yov:
		scp_049_y += scp_049_ych
	if not scp_106_xov:
		scp_106_x += scp_106_xch
	if not scp_106_yov:
		scp_106_x += scp_106_ych
	scp_106_x += player_xch
	scp_049_x += player_xch
	scp_049_xch = 0
	scp_106_xch = 0
	#draw most scps
	move(scp_049,scp_049_x,scp_049_y)
	move(scp_106,scp_106_x,scp_106_y)
	scp_049_xov = False
	done = False
	clock.tick(60)
	pygame.display.update()
	lx += player_xch
	ly += player_ych
	if wait == 0:
		wait = 180
#quit
pygame.quit()
quit()
