# scp-2d
SCP-2D game

Similar to SCP Containment Breach, but before the breach, and also it's 2D.

**NOTICE: SCP-2D IS BEING REMADE IN Defold! THIS REPOSITORY IS NO LONGER IN USE AND THE NEW VERSION CAN BE FOUND AT https://gitlab.com/FamiliarBreakfast/SCP-2DE/**
This repository will remain for historical preservation.

![](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)  
Creative Commons License  
This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)

To use, run engine.py with Python3. It also requires pygame.
This game is not canon and may not be completely accurate to the SCP lore.

This is not a demo. This is a testing build. It may contain features that might not be in the final game.

Disclaimer: The code for this game is a f***ing mess. There is no standardization, doesn't use classes, instead making its own system, and defies most programming standards.

Only SCPs that can be contained inside a building will be added. SCPs that cannot be moved or cannot be contained in a building will not be added. Do not request for them to be added.

Currently Done:
- Basic game engine
- Player sprite
- Sprite drawing
- SCP-173
- SCP-106
- SCP-049
- Doors!
- Buttons

TODO:
- Make more sprites
- Improve backgrounds
- Make more rooms
- Finish engine
- Everything else
- Multiplayer?

Version 0.51
- Added button function
- Fixed a bug where the doors would break after spamming left/right

Version 0.5 - Deadlands
- Button support has been added
- You can now die
- More comments

Version 0.4 - The Cure Update:
- SCP-106 can now walk through doors
- Added SCP-049
- Added a health system. You now have 5 health points. Lose them all and nothing happens yet. 173 kills instantly
- Added a flashing invincibility period
- Made Out-Of-Bounds black instead of white

Version 0.31:
- Made SCP-106 unable to walk through doors (temp, just for creating AI collisions)

Version 0.3 - The Insanity Update:
- Added functioning doors (player only)
- Fixed a bug where an elif statement was getting executed regardless of whether or not it should because f*** you. Then I built my own damn elif statement and suprise suprise, it worked.
- Lost my sanity about twice
- TIL Python is a stupid language that can't even figure out if-else statements and now I somewhat regret making this game in Python.

Version 0.22:
- Fixed a bug where SCP-106 would snap to you
- Fixed a bug where SCP-173 flipped while walking left

Version 0.21:
- Added an actual background
- Fixed a bug where SCP-106 shook while standing on you
- Added a bug where SCP-106 jumps in front of you
- Added a bug where SCP-173 jumps in front of you

Version 0.2:
- Added SCP-106
- Added a temporary background
- Added camera """scrolling"""
- Fixed a bug where SCP-106 froze	

Version 0.121:
- Fixed a bug where SCP-173 wouldn't move to the player if the player was to the left of 173
- Fixed a bug where the player couldn't turn around after going off screen

Version 0.12:
- Added SCP-173
- Fixed a bug where 173 would zoom around at ever increasing speeds
- Fixed a bug where the players temporary speed variables were never cleared
- Removed crash handler debug messages
- Added the ability to turn around*
- Fixed logic issues in handling player direction*

*Player direction always existed, it just has a sprite and purpose now

Version 0.11:
- Bugfix
- Added sprite
